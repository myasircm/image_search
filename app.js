
const express = require('express');
const http_request = require('request');
const vision = require('node-cloud-vision-api');
var config = require('./config.js');
var app = express();
var expressLayouts = require('express-ejs-layouts');
var imageDiff = require('image-diff');
var fs = require('fs');
var async = require('async');
var extend = require('extend');

app.set('view engine', 'ejs');
app.use(expressLayouts);
app.set('layout extractScripts', true);
app.set('layout extractStyles', true);


vision.init({auth: 'AIzaSyAG9K4K5d8HSKfYNLnKKzDIcY07q-pg-FE'});

var headers = {
    'Content-Type':'application/json'
};

var getQuery = function(req, search_keyword) {
	var options = {
	    url: 'http://lb-e-brm-mob1-core-0-231514609.us-east-1.elb.amazonaws.com/api/v1/core/',
	    method: 'GET',
	    headers: headers,
	    qs: {}
	}
	var query = {
		account_id: Number(req.query.account_id),
		request_id: 8111638310813,
		_br_uid_2: "uid%3D6283631823848%3Av%3D11.8%3Ats%3D1497871207007%3Ahc%3D4",
		url: "www.bloomique.com",
		ref_url: "www.bloomique.com",
		request_type: "search",
		fl:  "pid ,title ,brand ,price ,sale_price ,promotions ,thumb_image ,sku_thumb_images ,sku_swatch_images ,sku_color_group ,url ,price_range ,sale_price_range ,description ,is_live ,score",
		search_type: "keyword",
		rows: 10,
		start: 0,
		"facet.limit": 10,
		domain_key: req.query.domain,
		q: search_keyword
	}
	options.qs = query;
	return options;
}

app.get('/get_products', function (req, res) {
	const vision_request = new vision.Request({
			image: new vision.Image({
				url: req.query.image
	  		}),
		  	features: [
			    new vision.Feature('WEB_DETECTION', 10),
		//	    new vision.Feature('LABEL_DETECTION', 1),
			    new vision.Feature('LOGO_DETECTION', 2)
		  	]
	});
	vision.annotate(vision_request).then((response) => {
		var sorted_response = sortAndFilter(response.responses[0]);
		var search_keyword = searchKeyword(sorted_response);

		var fe_query = getQuery(req, search_keyword);

		console.log(JSON.stringify(fe_query));

		http_request(fe_query, function(error, snap_response_status, snap_response_body) {
			if(!error && snap_response_status.statusCode == 200) {
				var image_list = fetchImages(JSON.parse(snap_response_body));
				sortImages(image_list, req.query.image, function(sorted_list, error) {
					if(!error) {
						var locals = {
						    title: 'Image Search',
						    message: 'Similar Products',
						    image_list: sorted_list,
						    original_image: req.query.image
						};
						res.render('view', locals);
					}
				})
			}
		});
	}, (e) => {
	  res.send(e);
	})
});

var makeSnapRequest = function(search_keywords, req, res) {

	async.parallel(search_keywords, function(keyword, cb) {
		var fe_query = getQuery(req, keyword);
		var responses = [];
		http_request(fe_query, function(error, snap_response_status, snap_response_body) {
			if(!error && snap_response_status.statusCode == 200) {
				responses.push.apply(JSON.parse(snap_response_body));
			}
		});
	}, function(err) {

	});

		http_request(fe_query, function(error, snap_response_status, snap_response_body) {
			if(!error && snap_response_status.statusCode == 200) {
				var image_list = fetchImages(JSON.parse(snap_response_body));
				sortImages(image_list, req.query.image, function(sorted_list, error) {
					if(!error) {
						var locals = {
						    title: 'Image Search',
						    message: 'Similar Products',
						    image_list: sorted_list,
						    original_image: req.query.image
						};
						res.render('view', locals);
					}
				})
			}
		});
}

var sortAndFilter = function(results) {
	var attribute_score_list = [];
	var attribute_list = [];
	var logo_list = results.logoAnnotations ? results.logoAnnotations : [];
	var label_list =  results.labelAnnotations ? results.labelAnnotations : [];
	var web_entity_list = results.webDetection ? results.webDetection.webEntities : [];
	console.log('logo list ' + JSON.stringify(logo_list));

	var attribute_score_list = label_list.concat(logo_list, web_entity_list);

	attribute_score_list.sort(function(a, b) {
    	return parseFloat(b.score) - parseFloat(a.score);
	});

	for (var i = 0; i < attribute_score_list.length; i++) {
		if(attribute_score_list[i].description && attribute_score_list[i].score >= config.score)
			attribute_list.push(attribute_score_list[i].description);
	}
	return attribute_list;
}

var searchKeyword = function(keywords) {
	var keyword = "";
	var i = 0, j = 0;
	while(j < Math.min(config.max_keywords, keywords.length) && i < keywords.length) {
		if(keyword.indexOf(keywords[i]) == -1) {
			keyword += keywords[i] + ' ';
			j++;
		}
		i++;
	}
	return keyword;
}

var fetchImages = function(response) {
	console.log('in fetchImages');
	response = response.response;
	var image_list = [];
	for(var i = 0; i < response.docs.length; i++) {
		if(response.docs[i].thumb_image) {
			var item = response.docs[i];
			item.index = i;
			item.price_string = "Price $" + response.docs[i].price;
			image_list.push(item);
		}
	}
	return image_list;
}

var sortImages = function(image_list, original_image, callback) {
	console.log('in sortImages');
	var result_array = [];
	downloadImage(original_image, 'original_image', function(){
		async.eachLimit(image_list, image_list.length, function(image, cb) {
			downloadImage(image.thumb_image, 'downloaded_image_' + image.index, function() {
				imageDiff.getFullResult({
					  actualImage: 'original_image',
					  expectedImage: 'downloaded_image_' + image.index,
				}, function (err, result) {
				  // result = {total: 46340.2, difference: 0.707107}
				    if(err) {
				  		console.log("error" + JSON.stringify(err));
				  		cb(err)
				    } else {
					  	image.difference = 1 - (result.percentage ? result.percentage : 1);
					  	result_array.push(image);
					  	cb();
				    }
				})
				
			})
		}, function(err) {
			if(!err) {
				result_array = result_array.filter(function(a) {
					console.log(JSON.stringify(parseFloat(a.difference)));
					if(parseFloat(a.difference) < config.similarity_score) {
						return false;
					} else 
						return true;
				})
				result_array.sort(function(a, b) {
					return (a.difference < b.difference);
				})
			}
			return callback(result_array, null);
		});
	})

}

var downloadImage = function(uri, filename, callback){
  http_request.head(uri, function(err, res, body){
    http_request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

app.listen(3000, function () {
  console.log('Image search app listening on port 3000!')
})