module.exports = {
	score: .3,
	threshold_score: {
		web: .8,
		label: .8,
		logo: .8
	},
	max_keywords: 3,
	min_keywords: 2,
	similarity_score: .1
}